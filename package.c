// First Fit Decreasing algorithm.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "head.h"

extern int genOrderAmount();
order orders[MAX_ORDER];

// Returns number of bins required using first fit 
// decreasing offline algorithm
 int container[MAX_CART][MAX_ORDER]={0};

int firstFit(int weight[], int n, int c)
{
	int i;
	
			// sort(weight, weight+n, std::greater<int>());
    // Initialize result (Count of bins)
    int res = 0;
 
    // Create an array to store remaining space in bins
    // there can be at most n bins
    int bin_rem[n];
   
 
    // Place items one by one
    for (i=0; i<n; i++)
    {
        // Find the first bin that can accommodate
        // weight[i]
        int j;
        for (j=0; j<res; j++)
        {
            if (bin_rem[j] >= weight[i])
            {
                bin_rem[j] = bin_rem[j] - weight[i];
                printf("container %d has %d\n", j, weight[i]);
                container[j][i] = weight[i];
                break;
            }
        }
 
        // If no bin could accommodate weight[i]
        if (j==res)
        {
            bin_rem[res] = c - weight[i];
            printf("container %d has %d\n", res, weight[i]);
 			container[res][i] = weight[i];
            res++;
        }
    }
		
	return res;
}


int firstFitDec(int weight[], int n, int c)
{
    int i,j,temp;   
    for(i = 0; i < n-1; i++)  
    {  
        for(j = i+1; j < n; j++){  
            if(weight[i]<weight[j])
			{  
                temp=weight[j];  
                weight[j]=weight[i];  
                weight[i]=temp;  
            }  
        }  
    }  
    return firstFit(weight, n, c);
}
 
// Driver program

int package(order orders[], unsigned short int currAmount)
{
	srand((unsigned) time(NULL));
	int weight[]={1,3,7,3,6,1,9,2,4};
	int i;
	
	printf("curramount:%d\n", currAmount);
	//copy orders.volume to weight[]
	for(i = 0;i<currAmount;i++)
	{
		weight[i] = orders[i].volume;
	
	}
	
  
	int c = 10;
    int n = sizeof(weight) / sizeof(weight[0]);
	int res = firstFitDec(weight, n, c);
	//printf("RES:%d\n", res);
    
    
    
     /*
    							 for(int p = 0;p<10;p++)
								    {
								    	for (int q = 0;q<10;q++)
								    	{
								    		printf("%d  ", container[p][q]);
										}
										printf("\n");
									}
	
     */
     
   //cout << "Number of bins required in First Fit "
     //   << "Decreasing : " << firstFitDec(weight, n, c);

    return res;
}


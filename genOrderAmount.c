#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "head.h"


double F(double x)
	{
	   double f;
	 // y=1.44E-12x^{6} - 1.068E-09x^{5} + 3E-07x^{4} - 3.916E-05x^{3} + 0.0022x^{2} - 0.0309x + 0.3756
	   /*f = pow(1.44, -12)*pow(x, 6)-
	   	   pow(1.068, -9)*pow(x, 5)+
	   	   pow(3, -7)*pow(x, 4)-
	   	   pow(3.916, -5)*pow(x, 3)+
	   	   0.0022*pow(x, 2)-
	   	   0.0309*pow(x, 1)+
	   	   0.3756;*/
	   	   f = 1.44*pow(10,-12)*x*x*x*x*x*x-1.068*pow(10, -9)*x*x*x*x*x+3*pow(10, -7)*x*x*x*x-3.916*pow(10,-5)*x*x*x+0.0022*x*x-0.0309*x+0.3756;
	   return(f);
	}
	

double Simpson(double a,double b,double eps, double (*P)(double)) 
{
	int n,k;
	double h,t1,t2,s1,s2,ep,p,x;
	n=1; h=b-a;
	t1=h*(P(a)+P(b))/2.0; 
	s1=t1;
	ep=eps+1.0;
	while (ep>=eps)
	{
	p=0.0;
	  for (k=0;k<=n-1;k++)
	  {
	   x=a+(k+0.5)*h;
	   p=p+P(x);
	  }
	  t2=(t1+h*p)/2.0;
	  s2=(4.0*t2-t1)/3.0;
	  ep=fabs(s2-s1);
	  t1=t2; s1=s2; n=n+n; h=h/2.0;
	}
	    return(s2);
}

int genOrderAmount(double a,double b)
{
	int num=Simpson(a,b,0.01,F);
	return num;
}

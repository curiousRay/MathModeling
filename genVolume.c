#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "head.h"


int genVolume()
{
	srand((unsigned) time(NULL));
	float p1=rand()/(RAND_MAX+1.0);
	float p2=rand()/(RAND_MAX+1.0);
	float p3=rand()/(RAND_MAX+1.0);
	return (int)(p1*5+p2*4+p3*8);
}

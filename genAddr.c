#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "head.h"

int genAddrX()
{
	srand((unsigned) time(NULL));
	unsigned short int x = rand() % 11;
	return x;
}

int genAddrY()
{
	srand((unsigned) time(NULL));
	unsigned short int y = rand() % 13;
	return y;
}
